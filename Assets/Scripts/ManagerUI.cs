using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerUI : MonoBehaviour
{
    [SerializeField] GameObject DeleteLastButton;
    [SerializeField] GameObject ContinueButton;
    [SerializeField] GameObject OkButton;

    [SerializeField] GameObject firstMessage;
    [SerializeField] GameObject secondMessage;
    bool isFirstMessage, isSecondMessage;
    public static ManagerUI instance;

    void Start()
    {
        instance = this;
        StartProperties();
    }
    void StartProperties()
    {
        firstMessage.SetActive(true);
        secondMessage.SetActive(false);
        DeleteLastButton.SetActive(false);
        ContinueButton.SetActive(false);
        OkButton.SetActive(true);
        isFirstMessage = true;
        isSecondMessage = false;
    }

    public void MouseClick(string _uid)
    {
        switch(_uid)
        {
            case "restart":
                RestartClick();
            break;
            case "continue":
                ContinueClick();
            break;
            case "delete":
                DeleteLastClick();
            break;
            case "ok":
                OkClick();
            break;
        }
    }
    public void RestartClick()
    {
        Main.instance.isDraw = false;
        Main.instance.Restart();
        StartProperties();
        Debug.Log("RestartBtn");
    }
    public void DeleteLastClick()
    {
        Main.instance.DeleteLastPoint();
        Debug.Log("DeleteLastPointBtn");
    }
    public void ContinueClick()
    {
        if (isFirstMessage && Main.instance.perimetrPointsCoord.Count >= 2)
        {
            Main.instance.isDraw = false;
            isFirstMessage = false;
            isSecondMessage = true;
            firstMessage.SetActive(false);
            secondMessage.SetActive(true);
            DeleteLastButton.SetActive(false);
            ContinueButton.SetActive(false);
            OkButton.SetActive(true);
            
        }
        else if (isSecondMessage)
        {
            Main.instance.isDraw = false;
            isSecondMessage = false;
            secondMessage.SetActive(false);
            DeleteLastButton.SetActive(false);
            ContinueButton.SetActive(false);
            OkButton.SetActive(false);
            Main.instance.Calculate();
            Poly.instance.CreateSurfaces(Main.instance.perimetrPointsCoord, Main.instance.heightDist);
        }

        Debug.Log("ContinueBtn");
    }
    public void OkClick()
    {
        if (isFirstMessage)
        {          
            Main.instance.InputPerimetrPoints();
        }
        if (isSecondMessage)
        {          
            Main.instance.InputHeightPoints(); 
        }
        Main.instance.isDraw = true;
        DeleteLastButton.SetActive(true);
        ContinueButton.SetActive(true);
        OkButton.SetActive(false);
        Debug.Log("OkBtn");
    }
}

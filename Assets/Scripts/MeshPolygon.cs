using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshPolygon : MonoBehaviour
{
    [SerializeField] Material material;
   
    public void CreatePolygon(List<Vector3> coordinates)
    {
        Vector2[] vertices2D = new Vector2[coordinates.Count];
        for (int i = 0; i < vertices2D.Length; i++)
        {
            vertices2D[i] = new Vector2(coordinates[i].x, coordinates[i].y);
        }
        Triangulator tr = new Triangulator(vertices2D);
        int[] indices = tr.Triangulate();

        Vector3[] vertices = new Vector3[vertices2D.Length];
        for (int i = 0; i < vertices.Length; i++)
        {
            vertices[i] = new Vector3(vertices2D[i].x, vertices2D[i].y, coordinates[i].z);
        }
        Mesh msh = new Mesh();
        msh.vertices = vertices;
        msh.triangles = indices;
        msh.RecalculateNormals();
        msh.RecalculateBounds();
        gameObject.AddComponent(typeof(MeshRenderer));
        MeshFilter filter = gameObject.AddComponent(typeof(MeshFilter)) as MeshFilter;
        gameObject.GetComponent<MeshRenderer>().sharedMaterial = material;
        filter.mesh = msh;

    }
    }

    

﻿using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using Color = UnityEngine.Color;
using Microsoft.MixedReality.Toolkit.UI.PulseShader;

public class Main : MonoBehaviour
{
    public Camera camera;
    public List<Vector3> perimetrPointsCoord = new List<Vector3>();
    private List<Vector3> heightPointsCoord = new List<Vector3>();
    private List<GameObject> allDrawObjects = new List<GameObject>();
    private bool isDrawPerimetrPoints = false;
    private bool isDrawHeightPoints = false;
    public float heightDist;
    public static Main instance;
    public bool isDraw = false;
    public Material material;

    void Awake()
    {
        instance = this;
    }


    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //CheckMesh(Input.mousePosition);
        }
        if (Input.GetMouseButtonUp(0))
        {
            GlobalClick(Input.mousePosition);
        }
    }

    private void CheckMesh(Vector3 _position)
    {
        /*Ray ray = camera.ScreenPointToRay(_position);
        RaycastHit hit;
        MeshRenderer meshRenderer;
        MeshCollider meshCollider;
        MeshFilter meshFilter;
        if (Physics.Raycast(ray, out hit))
        {
            Debug.Log("HIT game object: " + hit.collider.gameObject.name);
            meshRenderer = hit.collider.gameObject.GetComponent<MeshRenderer>();
            meshCollider = hit.collider.gameObject.GetComponent<MeshCollider>();
            meshFilter = hit.collider.gameObject.GetComponent<MeshFilter>();
            if (meshRenderer)
            {
                Debug.Log("________________________________");
                //meshRenderer.material = material;
                
            }
            if (meshCollider)
            {
                /*Mesh mesh = meshCollider.sharedMesh;
                Vector3[] vertices = mesh.vertices;
                int[] triangles = mesh.triangles;
                Vector3 p0 = vertices[triangles[hit.triangleIndex * 3 + 0]];
                Vector3 p1 = vertices[triangles[hit.triangleIndex * 3 + 1]];
                Vector3 p2 = vertices[triangles[hit.triangleIndex * 3 + 2]];
                Transform hitTransform = hit.collider.transform;
                p0 = hitTransform.TransformPoint(p0);
                p1 = hitTransform.TransformPoint(p1);
                p2 = hitTransform.TransformPoint(p2);
                MakeLineBetweenTwoPoint(p0, p1);
                MakeLineBetweenTwoPoint(p1, p2);
                MakeLineBetweenTwoPoint(p2, p0);
                //Debug.Log(meshCollider.sharedMesh.triangles.Count());
                //Debug.Log(meshCollider.sharedMesh.vertices.Count());
            }
            if (meshFilter)
            {
                Debug.Log(meshFilter.mesh.triangles.Length);
                Debug.Log(meshFilter.mesh.vertices.Length);
            }
        }*/

    }

    private void GlobalClick(Vector3 _position)
    {
        Ray ray = camera.ScreenPointToRay(_position);
        RaycastHit hit;
        DefaultButton button;
        if (Physics.Raycast(ray, out hit))
        {
            button = hit.collider.gameObject.GetComponent<DefaultButton>();
            if (button)
            {
                ManagerUI.instance.MouseClick(button.uid);
            }
            else
            {
                ClickWorld();
            }
        }
    }

    private int FindVertex(Vector3 _position, int[] _vertices)
    {
        int num = 0;

        return num;
    }


    private void ClickWorld()
    {
        if (isDraw == true)
        {
            if (isDrawHeightPoints == true || isDrawPerimetrPoints == true)
            {
               RaycastHit hit;
               Ray ray = camera.ScreenPointToRay(Input.mousePosition);

               if (Physics.Raycast(ray, out hit))
               {
                    AddPoint(hit.point);
               }
            }
        }
    }


    /// <summary>
    /// Mode floor points
    /// </summary>
    public void InputPerimetrPoints()
    {
        isDrawPerimetrPoints = true;
        isDrawHeightPoints = false;
    }
    /// <summary>
    /// Mode ceiling points
    /// </summary>
    public void InputHeightPoints()
    {
        isDrawPerimetrPoints = false;
        isDrawHeightPoints = true;
    }

    /// <summary>
    /// Delete all points
    /// </summary>
    public void Restart()
    {
        ClearAllDrawObjects();
        isDrawPerimetrPoints = false;
        isDrawHeightPoints = false;
        isDraw = false;
        perimetrPointsCoord.Clear();
        heightPointsCoord.Clear();
    }

    public void DeleteLastPoint()
    {
        Debug.Log("Delete last point");

        if (isDrawPerimetrPoints == true)
        {
            if (perimetrPointsCoord.Count > 0)
            {
                perimetrPointsCoord.RemoveAt(perimetrPointsCoord.Count - 1);
                ClearLastObject();
            }
        }
        if (isDrawHeightPoints == true)
        {
            if (heightPointsCoord.Count > 0)
            {
                heightPointsCoord.RemoveAt(heightPointsCoord.Count - 1);
                ClearLastObject();
            }
        }
        if (isDrawPerimetrPoints == true || isDrawHeightPoints == true) isDraw = true;
    }

    private void AddPoint(Vector3 _point)
    {
        if (isDrawPerimetrPoints == true) perimetrPointsCoord.Add(_point);
        if (isDrawHeightPoints == true)
        {
            if (heightPointsCoord.Count > 0) DeleteLastPoint();
            heightPointsCoord.Add(_point);
        }
        MakePoint(_point); 
    }

    public void Calculate()
    {
        if (perimetrPointsCoord.Count > 1 && heightPointsCoord.Count > 0) CalculateHeight();
    }

    private void CalculateHeight()
    {
        List<Vector3AndDistance> allDistance = new List<Vector3AndDistance>();
        //We are looking for the closest 2 of all perimeter points
        foreach (Vector3 point in perimetrPointsCoord)
        {
            float distance = Vector3.Distance(heightPointsCoord[heightPointsCoord.Count - 1], point);
            Vector3AndDistance vec = new Vector3AndDistance(point, distance);
            allDistance.Add(vec);
        }
        List<Vector3AndDistance> sortedVectors = allDistance.OrderBy(u => u.distance).ToList();
        //We conduct a perpendicular to the found vector x1 = > x2
        Vector3 perpedikular = Ortho(sortedVectors[0].point.x, sortedVectors[0].point.y, sortedVectors[0].point.z, sortedVectors[1].point.x, sortedVectors[1].point.y, sortedVectors[1].point.z, heightPointsCoord[heightPointsCoord.Count - 1].x, heightPointsCoord[heightPointsCoord.Count - 1].y, heightPointsCoord[heightPointsCoord.Count - 1].z);
        //Consider the length of the perpendicular
        heightDist = Vector3.Distance(perpedikular, heightPointsCoord[heightPointsCoord.Count - 1]);
        //Draw the vector strictly upwards from each perimeter point according to the length of the perpedicular
        DrawVectorsFromPerimeters();
        //DrawMeshes();
    }

    private void DrawMeshes()
    {
        for (int i=0; i<perimetrPointsCoord.Count-1; i++)
        {
            Vector3 pointBegin = perimetrPointsCoord[i];
            Vector3 pointEnd = new Vector3();
            pointEnd.x = pointBegin.x;
            pointEnd.y = pointBegin.y + heightDist;
            pointEnd.z = pointBegin.z;
            Vector3 pointBegin2 = perimetrPointsCoord[i+1];
            Vector3 pointEnd2 = new Vector3();
            pointEnd2.x = pointBegin2.x;
            pointEnd2.y = pointBegin2.y + heightDist;
            pointEnd2.z = pointBegin2.z;
            DrawMesh(pointBegin, pointBegin2, pointEnd, pointEnd2);
        }
    }
    private void DrawMesh(Vector3 p1, Vector3 p2, Vector3 p3, Vector3 p4)
    {
        MeshFilter m_f = GetComponent<MeshFilter>();
        Mesh mesh = new Mesh();
        m_f.mesh = mesh;
        Vector3[] vert = new Vector3[4];
        vert[0] = p1;
        vert[1] = p2;
        vert[2] = p3;
        vert[3] = p4;

        int[] tri = new int[6];
        tri[0] = 0;
        tri[1] = 2;
        tri[2] = 1;
        tri[3] = 2;
        tri[4] = 3;
        tri[5] = 1;

        mesh.vertices = vert;
        mesh.triangles = tri;
    }

    private void DrawVectorsFromPerimeters()
    {
        //Draw vectors to up
        foreach(Vector3 pointBegin in perimetrPointsCoord)
        {
            Vector3 pointEnd = new Vector3();
            pointEnd.x = pointBegin.x;
            pointEnd.y = pointBegin.y + heightDist;
            pointEnd.z = pointBegin.z;
            MakeLineBetweenTwoPoint(pointBegin, pointEnd);
        }
    }


    private void ClearAllDrawObjects()
    {
        foreach(GameObject obj in allDrawObjects)
        {
            Destroy(obj);
        }
        allDrawObjects.Clear();
        perimetrPointsCoord.Clear();
        heightPointsCoord.Clear();
    }
    private void ClearLastObject()
    {
        if (allDrawObjects.Count > 0)
        {
            Destroy(allDrawObjects[allDrawObjects.Count - 1]);
            allDrawObjects.RemoveAt(allDrawObjects.Count - 1);
        }
    }

    private void MakePoint(Vector3 pointCoord)
    {
        var point = GameObject.CreatePrimitive(PrimitiveType.Quad);
        allDrawObjects.Add(point);
        point.transform.localScale = Vector3.one * 0.01f;
        point.transform.position = pointCoord;
        Debug.Log("Make POINT");
    }

    private void MakeLineBetweenTwoPoint(Vector3 inputPosA, Vector3 inputPosB)
    {
        var line = GameObject.CreatePrimitive(PrimitiveType.Quad);
        allDrawObjects.Add(line);
        Vector3 posC = ((inputPosB - inputPosA) * 0.5F) + inputPosA;
        float lengthC = (inputPosB - inputPosA).magnitude; 
        float sineC = (inputPosB.y - inputPosA.y) / lengthC;
        float angleC = Mathf.Asin(sineC) * Mathf.Rad2Deg;
        if (inputPosB.x < inputPosA.x) { angleC = 0 - angleC; }
        line.transform.position = posC;
        line.transform.localScale = new Vector3(lengthC, 0.01f, 0.01f);
        line.transform.rotation = Quaternion.Euler(0, 0, angleC);

    }

    public Vector3 Ortho(float x1, float y1, float z1,
                         float x2, float y2, float z2,
                         float x3, float y3, float z3)
    {
        var c = Math.Sqrt(Math.Pow(x1 - x2, 2) + Math.Pow(y1 - y2, 2) + Math.Pow(z1 - z2, 2));
        var b = Math.Sqrt(Math.Pow(x2 - x3, 2) + Math.Pow(y2 - y3, 2) + Math.Pow(z2 - z3, 2));
        var a = Math.Sqrt(Math.Pow(x3 - x1, 2) + Math.Pow(y3 - y1, 2) + Math.Pow(z3 - z1, 2));
        var p = (a + b + c) / 2;
        var h = 2 * Math.Sqrt(p * (p - a) * (p - b) * (p - c)) / c;
        if (a / c > b / c)
        {
            var c1 = Math.Sqrt(a * a - h * h);
            var k = c1 / c;
            var x = x1 + k * (x2 - x1);
            var y = y1 + k * (y2 - y1);
            var z = z1 + k * (z2 - z1);
            return new Vector3((float)x, (float)y, (float)z);
        }
        else
        {
            var c2 = Math.Sqrt(b * b - h * h);
            var k = c2 / c;
            var x = x2 + k * (x1 - x2);
            var y = y2 + k * (y1 - y2);
            var z = z2 + k * (z1 - z2);
            return new Vector3((float)x, (float)y, (float)z);
        }
        
    }
}

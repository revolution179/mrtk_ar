using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poly : MonoBehaviour
{
    public static Poly instance;
    [SerializeField] GameObject surface;
    void Start()
    {
        instance = this;
    }
    public void CreateSurfaces(List<Vector3> coordinates,float height)
    {
        List<Vector3> go;
        int n = coordinates.Count;
        go = new List<Vector3>();
        for (int i = 0; i < n; i++)
        {
            go.Add(new Vector3(coordinates[i].x, coordinates[i].y, coordinates[i].z));
        }
        CreatePoly(go);
        for (int i = 0; i < n - 1; i++)
        {
            go = new List<Vector3>();
            go.Add(new Vector3(coordinates[i].x, coordinates[i].y, coordinates[i].z));
            go.Add(new Vector3(coordinates[i].x, coordinates[i].y + height, coordinates[i].z));
            go.Add(new Vector3(coordinates[i + 1].x, coordinates[i + 1].y + height, coordinates[i + 1].z));
            go.Add(new Vector3(coordinates[i + 1].x, coordinates[i + 1].y, coordinates[i + 1].z));
            CreatePoly(go);
        }
        go = new List<Vector3>();
        go.Add(new Vector3(coordinates[n - 1].x, coordinates[n - 1].y, coordinates[n - 1].z));
        go.Add(new Vector3(coordinates[n - 1].x, coordinates[n - 1].y + height, coordinates[n - 1].z));
        go.Add(new Vector3(coordinates[0].x, coordinates[0].y + height, coordinates[0].z));
        go.Add(new Vector3(coordinates[0].x, coordinates[0].y, coordinates[0].z));
        CreatePoly(go);
        go = new List<Vector3>();
        for (int i = 0; i < n; i++)
        {
            go.Add(new Vector3(coordinates[i].x, coordinates[i].y + height, coordinates[i].z));
        }
        CreatePoly(go);

    }
   public void CreatePoly(List<Vector3> coordinates)
    {       
        var position = new Vector3(0, 0, 0);
        GameObject newSurface = Instantiate(surface, position, Quaternion.identity);
        newSurface.GetComponent<MeshPolygon>().CreatePolygon(coordinates);
    }
}
